/**
 * Tests for lazy-set
 */
;(function () {
  'use strict'

  /* imports */
  var test = require('tap').test
  var LazySet = require('../lazy-set')
  var specifier = require('specifier')
  var typeCheck = require('type-check').typeCheck

  var spec = {
    get: {
      required: true,
      constriants: [
        specifier.type('Function')
      ]
    }
  }

  var lazySetSpec = specifier(spec)

  function testGetter (key, callback) {
    callback(null, key)
  }

  var lazySet = new LazySet(testGetter)

  var testKey = 'test key'

  test('LazySet should be a function', function (t) {
    if (!typeCheck('Function', LazySet)){
      t.fail()
    }

    t.end()
  })

  test('lazySet should have a get function', function (t) {
    lazySetSpec(lazySet, function (error, result) {
      if (error) {
        t.fail()
      }

      t.end()
    })
  })

  test('lazySet should recall a value with its key', function (t) {
    lazySet.get(testKey, function (error, value) {
      if (value !== testKey) {
        t.fail()
      }

      t.end()
    })
  })
})()

