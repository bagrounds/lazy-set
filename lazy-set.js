/**
 * @module lazy-set
 */
;(function () {
  'use strict'

  var set = {}
  var instance

  /* exports */
  module.exports = LazySet
  
  /**
   *
   * @constructor
   * @alias lazy-set
   *
   * @param {Function} getter given a key and callback, fetches item
   */
  function LazySet (getter) {
   this.getter = getter 

   instance = this
  }

  // set prototype and constructor
  LazySet.prototype = Object.create(LazySet.prototype);
  LazySet.prototype.constructor = LazySet;

  LazySet.prototype.get = get

  /**
   * Returns the value identified by the provided key. If the value is not
   * present, uses getter to fetch data first.
   *
   * @param {String} key identifies a value
   * @param {Function} callback handles results
   */
  function get (key, callback) {
    // if the requested item isn't present, fetch it
    if (!set[key]) {
      instance.getter(key, function (error, data) {
        if (error) {
          callback(error)
          return
        }

        set[key] = data

        callback(error, data)
      })
      return
    }

    // if the requested item is present, return that
    callback(null, set[key])
  }
}) ()

